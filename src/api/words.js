import request from '@/utils/request.js';

export const getNewWordsList = () => {
  return request({
    url: '/admin/newwords/all',
    method: 'get',
  });
};
export const getStopWordsList = () => {
  return request({
    url: '/admin/stopwords/all',
    method: 'get',
  });
};

export const deleteNewWord = (data) => {
  return request({
    url: '/admin/newwords',
    method: 'delete',
    data: data,
  });
};

export const deleteStopWord = (data) => {
  return request({
    url: '/admin/stopwords',
    method: 'delete',
    data: data,
  });
};

export const addNewWord = (data) => {
  return request({
    url: '/admin/newwords',
    method: 'post',
    data: data,
  });
};

export const addStopWord = (data) => {
  return request({
    url: '/admin/stopwords',
    method: 'post',
    data: data,
  });
};
