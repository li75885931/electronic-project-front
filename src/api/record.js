import request from '@/utils/request.js';
export const getUsersList = () => {
  return request({
    url: '/admin/histories/all',
    method: 'get',
  });
};

export const getUserRecords = (data) => {
  return request({
    url: '/admin/histories',
    method: 'get',
    data: data,
  });
};
