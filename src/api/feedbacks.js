import request from '@/utils/request';

export const getFeedBacks = () => {
  return request({
    url: '/admin/feedbacks',
    method: 'get',
  });
};
