import { createRouter, createWebHashHistory } from 'vue-router';
import Layout from '@/layouts/index.vue';
import i18n from '@/locales';
const { global } = i18n;


// 静态路由：不需要判断权限的路由
export const constantRoutes = [
  {
    path: '/myLogin',
    name: 'MyLogin',
    component: () => import('@/views/myLogin/index.vue'),
    meta: {
      title: '登录',
    },
    hidden: true,
  },
  {
    path: '/401',
    name: '401',
    component: () => import('@/views/errorPage/401.vue'),
    hidden: true,
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/errorPage/404.vue'),
    hidden: true,
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index.vue'),
    hidden: true,
  },
  {
    path: '/index',
    name: 'ResultPage',
    component: () => import('@/views/index/index.vue'),
    // props: (route) => route.params,
    hidden: true,
  },
];

// 动态路由： 需要动态判断权限并通过addRoutes动态添加页面
export const asyncRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/search',
    name: 'Root',
    children: [
      {
        path: '/search',
        name: 'Search',
        component: () => import('@/views/search/index.vue'),
        meta: {
          title: global.t('route.home'),
          icon: 'icon-home',
          affix: true,
          noKeepAlive: true,
        },
      },
      {
        path: '/index',
        name: 'ResultPage',
        component: () => import('@/views/index/index.vue'),
        // props: (route) => route.params,
        hidden: true,
      },
      {
        path: '/center',
        name: 'Center',
        component: () => import('@/views/center/index.vue'),
        // props: (route) => route.params,
        hidden: true,
      },
      {
        path: '/prop',
        name: 'PropWords',
        component: () => import('views/propWords/index.vue'),
        hidden: true,
      },
      {
        path: '/records',
        name: 'records',
        component: () => import('@/views/records/index.vue'),
        hidden: true,
      },
      {
        path: '/feedbacks',
        name: 'feedbacks',
        component: () => import('@/views/feedbacks/index.vue'),
        // props: (route) => route.params,
        hidden: true,
      },
      {
        path: '/words',
        name: 'words',
        component: () => import('@/views/words/index.vue'),
        // props: (route) => route.params,
        hidden: true,
      },
    ],
  },
  {
    path: '/comp',
    component: Layout,
    name: 'Comp',
    meta: { title: global.t('route.components'), icon: 'icon-code' },
    children: [
      {
        path: '/element',
        name: 'ElementComp',
        component: () => import('@/views/element/index.vue'),
        meta: {
          title: global.t('route.eleComponents'),
          icon: 'icon-code',
        },
      },
      {
        path: '/iconPark',
        name: 'IconPark',
        component: () => import('@/views/icon/index.vue'),
        meta: {
          title: global.t('route.icons'),
          icon: 'icon-like',
        },
      },
      {
        path: '/chart',
        name: 'Chart',
        component: () => import('@/views/echarts/index.vue'),
        meta: {
          title: global.t('route.charts'),
          icon: 'icon-chart-line',
        },
        children: [
          {
            path: '/line',
            name: 'Line',
            component: () => import('@/views/echarts/line.vue'),
            meta: {
              title: global.t('route.lineChart'),
            },
          },
          {
            path: '/bar',
            name: 'Bar',
            component: () => import('@/views/echarts/bar.vue'),
            meta: {
              title: global.t('route.barChart'),
            },
          },
          {
            path: '/otherChart',
            name: 'OtherChart',
            component: () => import('@/views/echarts/other.vue'),
            meta: {
              title: global.t('route.mixedChart'),
            },
          },
        ],
      },
    ],
  },
  {
    path: '/errorPage',
    name: 'ErrorPage',
    component: Layout,
    meta: {
      title: global.t('route.errorPages'),
      icon: 'icon-link-cloud-faild',
    },
    children: [
      {
        path: '/404Page',
        name: '404Page',
        component: () => import('@/views/errorPage/404.vue'),
        meta: {
          title: global.t('route.page404'),
          icon: 'icon-link-cloud-faild',
        },
      },
      {
        path: '/401Page',
        name: '401Page',
        component: () => import('@/views/errorPage/401.vue'),
        meta: {
          title: global.t('route.page401'),
          icon: 'icon-link-interrupt',
        },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes,
});
// router.addRoute(route: asyncRoutes): () => void

// reset router
export function resetRouter() {
  router.getRoutes().forEach((route) => {
    const { name } = route;
    if (name) {
      router.hasRoute(name) && router.removeRoute(name);
    }
  });
}

export default router;
