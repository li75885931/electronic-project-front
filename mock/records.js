const data = [
  {
    username : 'admin1',
    query:{
      word : "苹果",
      rating : 4,
      comment :"有参考性"
    },
  },
  {
      username : 'admin2',
      query:{
        word : "例子",
        rating : 2,
        comment :"有参考性"
      },
  },
  {
      username : 'admin3',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
      username : 'admin4',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
      username : 'admin5',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
      username : 'admin6',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
      username : 'admin7',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
      username : 'admin8',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
      username : 'admin9',
      query:{
        word : "苹果",
        rating : 4,
        comment :"有参考性"
      },
  },
  {
    username: 'admin10',
    query: {
      word: '苹果',
      rating: 4,
      comment: '有参考性',
    },
  }



];
export default [
  {
    url: '/api/admin/histories/all',
    type: 'post',
    response() {
      return { code: 200, msg: 'success', data: data };
    },
  },
  {
    url: '/api/admin/histories',
    type: 'get',
    response() {
      return {
        code: 200,
        msg: 'success',
        data: [
          {
            using_time: '2023-12-04 10:29:17+00:00',
            seed_words: [
              {
                id: 'f72130a2-d6cc-48bd-b00f-25371989e911',
                name: '百',
              },
            ],
          },
          {
            using_time: '2023-12-04 10:29:17+00:00',
            seed_words: [
              {
                id: 'f72130a2-d6cc-48bd-b00f-25371989e911',
                name: '百',
              },
            ],
          },
        ],
      };
    },
  },
];
