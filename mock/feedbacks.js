export default [
  {
    //url:'http://127.0.0.1:8000/api/admin/feedbacks',
    url: '/api/admin/feedbacks',
    type: 'post',
    response() {
      return {
        code: 200,
        msg: 'success',
        data: [
          {
            username: 'lixinjie',
            query: [
              {
                word: '苹果',
                rating: 4,
                comment: '有参考性'
              },
              {
                word: '粒子',
                rating: 4,
                comment: '有参考性'
              }
            ],
            timestamp: '2023-12-01T10:04:00Z'
          },
          {
            username: 'lixiaojie',
            query: [
              {
                word: '苹果',
                rating: 4,
                comment: '有参考性'
              },
              {
                word: '粒子',
                rating: 4,
                comment: '有参考性'
              }
            ],
            timestamp: '2023-12-01T10:04:00Z'
          }
        ]
      };
    },
  },
];
