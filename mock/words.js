export default [
  {
    url: '/api/admin/newwords/all',
    type: 'post',
    response() {
      return {
        code: 200,
        msg: 'success',
        data: {
          new_words: [
            {
              id: 'f72130a2-d6cc-48bd-b00f-25371989e911',
              name: '百',
            },
            {
              id: 'f72130a2-d6cc-48bd-b00f-25371989e912',
              name: '百度',
            },
            {
              id: 'f72130a2-d6cc-48bd-b00f-25371989e913',
              name: '百度一下',
            },
          ],
        },
      };
    },
  },
  {
    url: '/api/admin/stopwords/all',
    type: 'post',
    response() {
      return {
        code: 200,
        msg: 'success',
        data: {
          new_words: [
            {
              id: 'f72130a2-d6cc-48bd-b00f-25371989e911',
              name: '停止',
            },
            {
              id: 'f72130a2-d6cc-48bd-b00f-25371989e912',
              name: '停止2',
            },
            {
              id: 'f72130a2-d6cc-48bd-b00f-25371989e913',
              name: '停止3',
            },
          ],
        },
      };
    },
  },
  {
    url: '/api/admin/feedbacks',
    type: 'post',
    response() {
      return {
        code: 200,
        msg: 'success',
        data: 'success',
      };
    },
  },
];
